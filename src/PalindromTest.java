import org.junit.Assert;
import org.junit.Test;

public class PalindromTest {

    final static Palindrom algorithm = new PalindromImpl();

    @Test
    public void emptyInputTrue(){
        final String input = "";
        Assert.assertTrue(algorithm.isPalindrom(input));
    }

    @Test
    public void nullInputFalse(){
        final String input = null;
        Assert.assertFalse(algorithm.isPalindrom(input));
    }


    @Test
    public void oneCharTrue(){
        final String input = "A";
        Assert.assertTrue(algorithm.isPalindrom(input));
    }

    @Test
    public void twoCharsTrue(){
        final String input = "BB";
        Assert.assertTrue(algorithm.isPalindrom(input));
    }

    @Test
    public void twoCharsFalse(){
        final String input = "BA";
        Assert.assertFalse(algorithm.isPalindrom(input));
    }

    @Test
    public void longInputFalse(){
        final String input = "ABCDDBCA";
        Assert.assertFalse(algorithm.isPalindrom(input));
    }


    @Test
    public void longInputTrue(){
        final String input = "ABCDDCBA";
        Assert.assertTrue(algorithm.isPalindrom(input));
    }

}
