public interface Palindrom {

    /**
     * Decides if the input string is a <a href="https://cs.wikipedia.org/wiki/Palindrom">palindrom</a>.
     * A palindrome is a word, sentence, or number that can be read in any direction (right to left or left to right)
     * and always has the same meaning. For simplicity, we assume that the string does not contain spaces.
     * @param input String to be checked. Null input is not a palindrom.
     * @return true if it is a palindrom, otherwise false
     */
    boolean isPalindrom(final String input);
}
